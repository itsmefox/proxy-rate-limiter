import path from "path";

export class CustomError extends Error{
    misc?: string
}

export default class Logger {
    // set up attributes
    private readonly pathname: string;
    private message: Array<string>
    private location: Error;


    constructor() {
        // set up basic info

        this.pathname = path.resolve(".")
            // to deal with windows paths fecking it all up
            .replace(/\\/g, "\\\\");


        this.message = []
    }

    error(error: Error) {
        this.processError(error)
        console.error(this.message.join('\n'))
        return
    }

    log(type:string, message:any, location: Error = new Error()){
        this.location = location

        switch(type){
            case "error":{
                return this.error(message as Error)
            }
            default:{
                return console.log(message)
            }

        }

    }

    private processError(error: Error) {
        // if there is a specific error message
        if ("misc" in error) {
            this.message.push((error as CustomError).misc)
        }
        // this is where we trace back to the source of the issue
        if (error.stack) {
            // replace all instances of the path
            error.stack = error.stack.replace(new RegExp(this.pathname, "gi"), '.');
            this.location.stack = this.location.stack.replace(new RegExp(this.pathname, "gi"), '.');

            // the actual error message does not contain teh actual location of teh issue, just what module caused it
            // so I am using the location var with is a new error() and stripping out what I want from taht and merging teh two together

            // split both into arrays
            let errorArray = error.stack.split("\n");
            let locationArray = this.location.stack.split("\n")

            let newError = [
                // first line from the error
                errorArray[0],
                // third line from teh location
                locationArray[2],
                // rest of the error
                ...errorArray.splice(1)
            ];

            this.message.push(newError.join("\n"))
        }
    }

}