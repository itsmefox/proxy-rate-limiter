import http from 'http'
import Queue from 'smart-request-balancer';
import axios, {AxiosRequestConfig, Method} from 'axios'
import {rules, interfaces, port} from "../data.json"
import Logger from "./Logger"

const logger = new Logger()
const queue = new Queue({rules: rules});

const onRequest = (client_req, client_res) => {
    let start = new Date().getTime()
    if(
        // if set to " allow all interfaces
        interfaces !== "*" &&
        // host is eiterh localhost or IP or a hostname
        !client_req.headers["host"].startsWith(interfaces)
    ){
        // ignore all such requests
        client_res.end()
        return
    }

    let toGet = client_req.headers["proxy-conf"]
    if(typeof toGet !== "string"){
        client_res.write('No proxy-conf header stringified object supplied');
        client_res.end()
        return
    }

    try{
        toGet = JSON.parse(toGet)
    }catch{
        client_res.write('proxy-conf nor parsable into object');
        client_res.end()
        return
    }

    let [config, category] = generateConfig(toGet, client_req)

    if(config.url === "" ){
        client_res.write('No url supplied in proxy-conf header');
        client_res.end()
        return
    }

    proxy(config, category,client_res, start)
}

class toGet extends Object {
    url: string;
    category?: string;
    method?: Method;
    headers?: any;
    config?: AxiosRequestConfig;
}

const generateConfig = (toGet: toGet, client_req):[AxiosRequestConfig, string] =>{
    // for the options object first
    let config:AxiosRequestConfig

    // allow a config objec tot be passed through
    if(typeof toGet.config !== "undefined"){
        config = toGet.config
    }else{
        let headers = toGet.headers || {}
        if(typeof toGet.headers === "undefined") {
            // Copy headers from request to proxy if headers are not in config
            delete client_req.headers["proxy-conf"]
            delete client_req.headers["host"]
            delete client_req.headers["connection"]
            headers = Object.assign({}, client_req.headers)
        }

        // basic configh
        config = {
            url: toGet.url || "",
            method: toGet.method || "get",
            responseType: 'stream',
            headers: headers,
        }
    }

    // sort out teh category here
    let category = toGet.category || "common"
    if(typeof rules[category] === "undefined"){
        category = "common"
    }

    return [config, category]
}

const proxy = (config, category, client_res, start) => {
    queue.request((retry) => sendRequest(retry,config,category), config.url, category)
        // our actual response
        .then(res => {
            logger.log("info",createLogData(config, category, start, res))
            res.headers["Transfer-Encoding"] = "chunked"
            client_res.writeHead(res.status, res.headers)
            res.data.pipe(client_res, {end: true})
        })
        .catch(error => logger.log("error",error));
}

const sendRequest = (retry,config,category) =>{
    return axios(config)
        .catch(error => {
            try {
                // We've got 429 - too many requests
                if (error.response.status === 429) {
                    // usually 300 seconds
                    return retry(rules[category].limit / rules[category].rate)
                }
            }catch (e) {
                //Do nothing and return
            }
            return error.response
        })
}

const createLogData = (config, category, startTime,res) =>{
    return {
        url: config.url,
        method: config.method,
        category: category,
        duration: new Date().getTime() - startTime,
        timestamp: startTime,
        status: res.status
    }
}

http.createServer(onRequest).listen(port);