# proxy-rate-limiter

This is a transparent node.js proxy for rate limiting outgoing requests.

I have several scripts that access the Guildwars2 API which is rate limited.  
I found several ways to manage rate limited requests but they all had to be for teh same running instance.  
By routing all teh requests through this I will be able to effectly keep within the limit.

## Own use.
If you want to use this yourself I would recommend forking it as I plan to add logging to my version, probably greylog or the like and I will be editing Logger.ts to suit

## "to-get" header

This server accepts all requests.  
It will look for a "to-get" header.  
The header should be a stringified object that should contain. 
```json
{
  "url":  "[required] (if config isnt set) - url of the request",
  "method": "[optional] - defaults to get",
  "category": "[optional] - defaults to common",
  "headers": "[optional] - object with additional headers. Ff not set the headers from the request will be copied over",
  "config": "[optional] - an axios config object, overrides other options"
}
```

## data.json
Currently contains two fields:

* ``rules`` - the same format as used by the ``smart-request-balancer``  
* ``interfaces`` - localhost means it can only accept requests on teh localhost interface, use "*" for all interfaces
